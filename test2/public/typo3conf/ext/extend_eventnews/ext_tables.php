<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('extend_eventnews', 'Configuration/TypoScript', 'ExtendEventnews');

    }
);




$tempColumns = Array (
    "public" => Array (
                    "exclude" => 1,
                    "label" => "LLL:EXT:extend_eventnews/Resources/Private/Language/locallang_db.xlf:public",
                    "config" => Array (
                                    "type" => "check",
                                    "renderType" => "checkboxToggle",
                                    "default" => "0",
                                    "items" => [
                                        [
                                            0 => "",
                                            1 => "",
                                        ]
                                    ],
                    )
    ),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns("tx_eventnews_domain_model_location",$tempColumns,1);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes("tx_eventnews_domain_model_location","public;;;;1-1-1");
?>