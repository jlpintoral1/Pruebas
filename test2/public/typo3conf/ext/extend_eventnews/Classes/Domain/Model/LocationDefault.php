<?php
namespace Icti\ExtendEventnews\Domain\Model;

class LocationDefault 
    extends \GeorgRinger\Eventnews\Domain\Model\Location
{
        /**
         * the new field public
         * @var boolean
         */
        protected $newPublic;
        
        /**
         * Returns an array of orderings created from a given demand object.
         *
         * @param boolean $newPublic
         * @return void
         */
        public function setNewPublic($newPublic) {
                $this->newPublic = $newPublic;
        }
        /**
         * Get new field public
         *
         * @return boolean
         */
        public function getNewPublic() {
                return $this->newPublic;
        }
}
?>