+++
title = "Contact"
+++

* Email: [jlpintoral1@gmail.com](mailto:jlpintoral@gmail.com)
* Teléfono móvil: [+34671092207](tel:+34671092207)

---

## Dirección Postal

> Santa Llogaia, 22 6B
>
> Figueres, Girona
>
> Spain
---

## en Redes Sociales

1. [Facebook](https://www.facebook.com/jpintoral)
2. [Twitter](https://twitter.com/jlpintoral1)
3. [GitHub](https://github.com/jlpintoral)
4. [Linkedin](https://www.linkedin.com/in/jlpin/)
