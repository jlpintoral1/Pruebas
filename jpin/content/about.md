+++
title = "Sobre"
+++

## Sobre mi

{{< figure class="avatar" src="https://s.gravatar.com/avatar/5438a9ff58e01a0841f483f2096c3f2d?s=80" >}}


## Extracto

En la actualidad estoy especializándome en desarrollos de web apps
seo optimizadas.
- Experto en sitios web dinámicos Typo3 y estáticos, Hugo y semi estáticos, Grav. 
- Administración de Servidores en la nube para empresas, correo,
web, dns, etc.

## Experiencia


### Freelance 
Figueres, Girona y remoto
octubre de 2008 - Hasta hoy.
- Administración de Servidores Web
- Servidores de Correo Electrónico, DNS.
- Servicios de asistencia técnica personalizada.
- Desarrollo de Sitios web avanzados con últimas tecnologías.
- SEO Posicionamiento y Redes Sociales.
- Desarrollos en entornos GIT (GITHUB, GITLAB)
- Instalador de servicios en AWS, Google Cloud. EC2, BEANSTALK, S3, ...
- Experto en TYPO3 cms.

### Seidor (BARCELONA)
Programador COBOL/DB2 en AS400
marzo de 2008 - septiembre de 2008 (7 meses)
- Programas cobol as400 para aplicación a medida para Instituciones Públicas.


### SOLUCIONS ENGINYERIA INFORMACIO SL (FIGUERES)
junio de 2003 - diciembre de 2004 (1 año 7 meses)
Figueres
- Administrador de Sistemas
- Senior Programmer (perl)
- administrador portal turístico CBHOLIDAYS
- Técnico en redes locales y formación de usuarios.

### SOL10, CBP (FIGUERES)
**CAFEVIRTUAL**
febrero de 1996 - diciembre de 1998 (2 años 11 meses)
plaça del sol 10 figueres girona
- ISP - Administrador sistemas en CafeVirual (2º cybercafe en España)
- Primer proveedor de servicios internet en la comarca.
- Cursos de iniciación a las nuevas tecnologías.

### NCR Corporation (BARCELONA)
septiembre de 1990 - septiembre de 1991 (1 año 1 mes)
- Técnico Comercial (Comercio e Industria)
- Sistemas host propiedad NCR (IMOS/ITX)
- Comercial de la compañía para toda la provincia de Girona de sistemas
mainframes NRC.

### DESENVOLUPAMENT DE TECNIQUES INFORMATIQUES S.A. (GIRONA)
julio de 1988 - diciembre de 1989 (1 año 6 meses).
- Programador COBOL (RM-85) - UNIX SYSTEM V (Santa Cruz Operation)
- Programa gestión inmobiliaria, materias primas, recambios, tiendas téxtil, y entornos de producción, entre otros. 


### Hackers Informatic Center (FIGUERES)
septiembre de 1984 - octubre de 1985 (1 año 2 meses)
- Programador COBOL ANSI, 4GL TRANSTOOLS
- Programas de gestión 
- Cursos de formación COBOL 74

### Gerauto SA (PEUGEOT-TALBOT GIRONA)
enero de 1983 - diciembre de 1984 (2 años)
- Programador COBOL-74 NCR IMOS - ITX
- Mantenimiento programas de contabilidad general y aplicación para concesionarios y
talleres, recambios de automoción. 
