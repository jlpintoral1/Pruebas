<?php
namespace Icti\IctiTest;


class ExampleTime {
	public function printTime(string $content, array $conf){
		
		$begin = $GLOBALS['TSFE']->fe_user->getKey('ses' ,"begin");
		if (!$begin){      
			$begin = time();   
			$this->getSession()->setKey("ses", 'begin' ,$begin);
			$GLOBALS['TSFE']->fe_user->storeSessionData();
		}

		return date("d M Y H:m:s",time(  )). ". llevas ".intval((time()-$begin)/60)." minutos conectado";
	}
	public function getSession(){
		return $GLOBALS['TSFE']->fe_user;
	}
}
?>

