<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Test extensions',
    'description' => 'Really dummy extension for testing purposes',
    'category' => 'fe',
    'author' => 'Ramírez',
    'author_email' => 'ramirez@icti.es',
    'state' => 'stable',
    'clearCacheOnLoad' => true,
    'version' => '0.0.1',
    'constraints' => [
        'depends' => [
            'typo3' => '9.5.17-10.9.99',
        ],
        'conflicts' => [],
    ],
];
